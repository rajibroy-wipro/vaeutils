var finalReportLink, downloadReportLink, clientID, clientSecret;
var goButton,goPrjID,summaryDiv,processingIcon,goIcon;
var accessToken,accessTokenURL,projectListURL;
var projectPage;
var proxyURL;
var projectTitles=[];
var projectIDs=[];

window.onload=function(){

	finalReportLink="";
	downloadReportLink="https://app.virtualautomationengineer.com/sites/admin/projectreport?project_id=";
	clientID="";
	clientSecret="";
	goPrjID=document.getElementById('goPrjID');
	goButton=document.getElementById('go');
	summaryDiv=document.getElementById('summaryData');
	processingIcon=document.getElementById('processingIcon');
	goIcon=document.getElementById('goIcon');
	accessToken="";
	proxyURL="https://young-shore-28061.herokuapp.com/";
	accessTokenURL="https://app.virtualautomationengineer.com/api/oapi/getAccessToken?";
	projectListURL="https://app.virtualautomationengineer.com/api/oapi/projectlisting?";
	projectPage="https://app.virtualautomationengineer.com/sites/testing/index/?project="

	if(localStorage['clientID']!==undefined && localStorage['clientID']!==null){
		clientID=localStorage['clientID'];
		$('#clientID').val(localStorage['clientID']);
	}

	if(localStorage['clientSecret']!==undefined && localStorage['clientSecret']!==null){
		clientSecret=localStorage['clientSecret'];
		$('#clientSecret').val(localStorage['clientSecret']);
	}

	if(localStorage['clientID']!==undefined && localStorage['clientID']!==null 
		&& localStorage['clientSecret']!==undefined && localStorage['clientSecret']!==null){
		goButton.disabled=false;
	}

	if(summaryDiv.classList.contains("show")){
        summaryDiv.classList.remove("show");
    }
    if(!summaryDiv.classList.contains("hide")){
        summaryDiv.classList.add("hide");
    }

	$('#projectID').on('input',onUserInput);
	$('#projectID').on('keyup',onEnterProjectID);
	$('#projectID').on('keypress',onlyNumberKey);
	$('#goPrjID').click(downloadReportByID);

	$('#clientID').on('input',onClientIDInput);
	$('#clientID').on('keyup',onEnterClientSecret);
	$('#clientSecret').on('input',onClientSecretInput);
	$('#clientSecret').on('keyup',onEnterClientSecret);

	$('#go').click(getAccessToken);

}

function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode < 48 || ASCIICode > 57)
            return false; 
        else{
        	return true; 
    	}
}

function onUserInput(evt){
	if(this.value.length>0){
			goPrjID.disabled=false;
	    	finalReportLink=downloadReportLink+this.value;
	}else{
			goPrjID.disabled=true;
	    	finalReportLink="";
	}
}

function downloadReportByID(){
	if(finalReportLink.includes(downloadReportLink))
		window.open(finalReportLink);
}

function onEnterProjectID(evt){
	if(evt.which===13 && finalReportLink.includes(downloadReportLink))
		window.open(finalReportLink);
}

function onClientIDInput(evt){
	if(this.value.length>0){
	    	clientID=this.value;
	    	if(document.getElementById('clientSecret').value.length>0)
	    		goButton.disabled=false;
	    	else
	    		goButton.disabled=true;
	}else{
	    	clientID="";
	    	goButton.disabled=true;
	}
}

function onClientSecretInput(evt){
	if(this.value.length>0){
	    	clientSecret=this.value;
	    	if(document.getElementById('clientID').value.length>0)
	    		goButton.disabled=false;
	    	else
	    		goButton.disabled=true;
	}else{
	    	clientSecret="";
	    	goButton.disabled=true;
	}
}

function onEnterClientSecret(evt){
	if(evt.which===13 && goButton.disabled===false)
		getAccessToken();
}

function processingAnim(){
	goButton.disabled=true;
	processingIcon.style.display="inline-block";
	goIcon.style.display="none";
}

function processingDoneAnim(){
	goButton.disabled=false;
	processingIcon.style.display="none";
	goIcon.style.display="inline-block";

}

function scrollToProjects(){
	$([document.documentElement, document.body]).animate({
        scrollTop: $("#aboveProjects").offset().top
    }, 1500);
}

function getAccessToken(){
	processingAnim();
	accessTokenURL+="apikey="+clientID+"&secret="+clientSecret+"&response_type=json";
	fetch(proxyURL+accessTokenURL)
	.then((resp) => resp.json())
	.then(function(data) {
    	accessToken=data[0]["access_token"];
    	getProjectList();
    })
	.catch(function(error) {
		processingDoneAnim();
	    console.log(error);
	}); 
}

function getProjectList(){
	if(accessToken!==undefined && accessToken!==null && accessToken!==""){
		projectListURL+="accesstoken="+accessToken+"&response_type=json";
		fetch(proxyURL+projectListURL)
		.then((resp) => resp.json())
		.then(function(data) {
			processingDoneAnim();
			if(data['RESULTSET']!==undefined && data['RESULTSET']!==null){
		    	data['RESULTSET'].forEach(function(item,index){
		    		projectTitles[index]=item['title'];
		    		projectIDs[index]=item['id'];
		    	});
		    	postRetreivingProjList();
		    }else{
		    	processingDoneAnim();
		    	showSnackBar("#D74825","fa-remove","Invalid Credentials!")
				console.log("Invalid Access Token!");
		    }
	    })
		.catch(function(error) {
			processingDoneAnim();
		    console.log(error);
		}); 
	}
	else{
		processingDoneAnim();
		showSnackBar("#D74825","fa-remove","Invalid Credentials!")
		console.log("Access Token not found!");
	}
}

function postRetreivingProjList(){
	localStorage['clientID']=clientID;
	localStorage['clientSecret']=clientSecret;

	$("#orderDetails tbody").empty();
	$.each(projectTitles, function(index,value){
		$('#orderDetails tbody').append("<tr><td onclick='copyText(this);' style='vertical-align: middle;width:15%'>"+projectIDs[index]+"</td><td style='vertical-align: middle;width:55%'><span onclick='window.open(\""+projectPage+projectIDs[index]+"\")'>"+value+"</span></td><td style='vertical-align: middle;width:15%'><i data-toggle='tooltip' title='Download' onclick='window.open(\""+downloadReportLink+projectIDs[index]+"\")' id='"+projectIDs[index]+"' class='downloadIcon fa fa-lg fa-download'></i></td><td style='vertical-align: middle;width:15%'><i data-toggle='tooltip' title='View' id='"+projectIDs[index]+"' class='downloadIcon fa fa-lg fa-eye'></i></td></tr>");
	});

	if(summaryDiv.classList.contains("hide")){
    	document.getElementById("summaryData").classList.remove("hide");
    }
    summaryDiv.classList.add("show");
    scrollToProjects();
    showSnackBar("#23cba0","fa-check","Loaded SUCCESSFULLY")			
}

function copyText(el){
  	var range = document.createRange();
    range.selectNodeContents(el);  
    var sel = window.getSelection(); 
    sel.removeAllRanges(); 
    sel.addRange(range);
    var successful = document.execCommand('copy');
    var msg = successful ? showSnackBar("#23CBA0","fa-check","COPIED SUCCESSFULLY") : showSnackBar("#D74825","fa-remove","UNABLE TO COPY");
}

function showSnackBar(bgcolor,icon,text) {
        var snackbarIcon = document.getElementById("snackbarIcon");
        var snackbarText = document.getElementById("snackbarText");

        snackbarIcon.classList.add(icon);
        snackbarText.innerHTML=text;

        var x = document.getElementById("snackbar");
        x.className = "show";
        x.style.backgroundColor=bgcolor;

        setTimeout(function(){ 
            x.className = x.className.replace("show", ""); 
            var snackbarIcon = document.getElementById("snackbarIcon");
            var snackbarText = document.getElementById("snackbarText");

            snackbarIcon.classList.remove(icon);
            snackbarText.innerHTML="";
        }, 1750);
}